# Für Entwickler

## Voraussetzungen für die Arbeit an diesem Projekt
- Wenn nötig, Linux (z.B. XUbuntu) in einer VM installieren
- ggf. aktuelle Quellen für unten stehende Pakete ermitteln und dem System hinzufügen
- Paket `docker-ce` installieren
- Paket `nodejs` installieren
- Paket `git` installieren
- Pakete für bevorzugte IDE installieren (z.B. "Visual Studio Code")
- Alias für `localhost` in `/etc/hosts` eintragen (z.B. `bsc.localhost.dev`)

## Vorbereitung der Entwicklungsumgebung
- dieses Git-Projekt klonen (`git clone ...`)
- aktuelle Version von Wordpress herunterladen und nach `docker_wordpress` entpacken
- Docker in den Swarm-Modus versetzen (`docker swarm init`)
- Docker-Umgebung erzeugen (`./docker/deploy.bash`)
- `docker/deploy.bash` nach Start aller Services mit `STRG+C` abbrechen
- Webseite im Browser mit oben vergebenem Alias (z.B. `http://bsc.localhost.dev`) aufrufen und Wordpress installieren (DB-Daten sind in `docker/stack.yml` ersichtlich)
- in den WP-Dateien einen symbolischen Link auf das Theme anlegen (`cd ./docker_wordpress/wp_content/themes && ln -sv ../../../src/bsc-glauchau-theme.dev bsc-glauchau-theme`)
- in Wordpress anmelden und BSC-Theme aktivieren

## Entwicklungs-Loop
- eigene IDE starten
- `./docker/up.bash` startet lokalen Webserver und Datenbankserver
- `cd src && npm run dev && cd ..` startet lokales Frontend
- ... {*tun, was immer zu tun ist*}
- ... {*bei Änderungen an Wordpress-Dateien, diese mit `npm run generate:wordpress` auf den lokalen Webserver deployen*}
- `STRG+C` beendet das lokale Frontend
- `./docker/down.bash` beendet lokalen Webserver und Datenbankserver

Serverlogs können durch Ausführung von `./docker/watch.bash` überwacht werden. Das Skript kann mit `STRG+C` wieder beendet werden.

## Deployment

- `cd src; npm run generate` erzeugt neues Theme-Verzeichnis mit aktuellen (statischen) Nuxt- und Wordpress-Dateien
