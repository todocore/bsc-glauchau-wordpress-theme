# Setup the server from scratch

## Install current version of Wordpress

### Add on top of `wp-config.php`
```
define( 'ALLOW_BSC_THEME', 'ALLOW_BSC_THEME' );
define( 'DISALLOW_FILE_EDIT', true );
define( 'WP_CACHE', true );
```

### Add to `.htaccess` (outside of WORDPRESS block)
```
<IfModule mod_rewrite.c>
  RewriteEngine On

  RewriteRule "^.*wp_config.php.*$" '-' [NC,F]

  RewriteRule "^.*\.cache.*$" '-' [NC,F]

  RewriteCond "%{REMOTE_ADDR}" "!=%{SERVER_ADDR}"
  RewriteRule "^.*(xmlrpc.php|wp-mail.php|wp-cron.php|wp-login).*$" '-' [NC,F]
</IfModule>
```

### Settings
- website title
- website subtitle
- admin email
- default role: `subscriber`
- time zone: `Berlin`
- date format: `j. F Y`
- time format: `G:i`
- week start: `Montag`

### Create page for privacy policy

### Upload media
- BSC Logo
- Hintergrundbild für Login-Screen

---
## Install plugins

### WPS Hide Login
- reconfigure login URL

### User Role Editor
- administrator => Administrator (everything)
- editor => Vorstand (CRUD content, Users)
- author => Inhalte verwalten (CRUD content)
- contributor => unbenutzt (nothing)
- subscriber => (read public and private)

### Simple JWT Login
- route name space: `simple-jwt-login/v1/`
- key source: `plugin settings`
- algorithm: `HS256`
- key: to be generated
- token: from headers `X-BSC-Authorization`
- `[X] All WordPress endpoints checks for JWT authentication `
- allow login by `email`
- `no redirect` after login
- allow register with auth code
- new user profile: `subscriber
- `[X] Generate a random password when a new user is created `
- allow authentication
- generate auth code (use it from frontend)
- activate hook `simple_jwt_login_register_hook`
- all other option to `no`

### Custom Login Page Customizer

#### Settings
- Login Order => `Only Email Adresses`
- Language Switcher => `Yes` (remove)

#### Layout
- Backgroundsize: `fill screen` / `no repeat`
---
- Logo size: `100x100 px`
- Logo bottom padding: `5 px`
- Logo URL: `/`
- Logo ttile: `Willkommen im internen Bereich`
---
- Form background color: `#fff`
- Form size: `320x200 px`
- Form padding: `26 24 46 24`
- Form border radius: `25`
- Form shadow spread: `12`
- Form shadow color: `rgba(0,0,0,0.13)`
---
- Field width: `100%`
- Field font size: `18`
- Field border width: `2`
- Field border color: `#ddd`
- Field border radius: `25`
- Field margin: `2 6 16 0`
- Field padding: `3 15 3 15`
- Field background color: `#fff`
- Field text color: `#000`
- Field label color: `#777`
- Field label font size: `14`
---
- Button background color: `#2EA2CC`
- Button hover color: `#1E8CBE`
- Button size: `auto`
- Button font size: `18`
- Button text color: `#fff`
- Button border width: `2`
- Button border color: `#0074A2`
- Button border hover: `#0074A2`
- Button shadow spread: `6`
- Button shadow color: `#78C8E6`
- Button text shadow: `#006799`
---
- Other options - lost passwort: `YES`
- Other option - privacy policy: `NO`
- Otehr option - back to website: `YES`
- Other font size: `14`
- Other text color: `#565656`
- Other hover color: `#ff3a41`

### Duplicator

### Folders
- Use Folders with: Medien
- Use keyboard shortcuts
- Use folders with undo (5 seconds)
- blue/black color schema

### Newsletter

### WP Mail Loggin
- activate cleanup by time (`29 days`)

### Pods
- import PODs from project

---
## Select BSC theme
