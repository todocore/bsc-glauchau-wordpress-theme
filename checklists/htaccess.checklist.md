# .htaccess

**Add on testsystenm**
```
AuthType Basic
AuthName "Passwort notwendig"
AuthUserFile {absolute file path on server}/.htpasswd
AuthGroupFile /dev/null
Require valid-user
```

use this to find absolute file path on server for `AuthUserFile`:
```
<?php
echo $_SERVER['SCRIPT_FILENAME'];
?>
```
