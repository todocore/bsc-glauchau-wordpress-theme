# WP_DEBUG
**set**
```
define( 'WP_DEBUG', true );
```
**or unset**
```
define( 'WP_DEBUG', false );
```

if this dows not help use this in addition:
```
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
```
