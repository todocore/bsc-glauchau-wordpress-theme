export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'BSC Glauchau e.V.',
    meta: [
      { charset: 'utf-8', },
      { name: 'viewport', content: 'width=device-width, initial-scale=1', },
      { hid: 'description', name: 'description', content: '', },
      { name: 'format-detection', content: 'telephone=no', },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: `${process.env.URL_WP_API}/wp-content/themes/bsc-glauchau-theme/favicon.ico`, },
    ],
  },

  env: {
    // this is only for development environment - see app.html and runtime store for WP integration
    URL_WP_API: process.env.URL_WP_API,
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/tailwind.css',
    '@/assets/css/bootstrap-icons.css',
    '@/assets/css/wp-content.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    'plugins/wordpressApi.js',
    'plugins/wordpressTools.js',
    'plugins/vuelidate.js',
    'plugins/fuzzy-search.js',
    'plugins/strict-search.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/svg',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/date-fns',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
    // credentials: true,
    // init (axios) {
    //   axios.defaults.withCredentials = true
    // }
  },

  dateFns: {
    locales: ['de',],
    defaultLocale: 'de',
    fallbackLocale: 'de',
  },

  router: {
    middleware: 'loginByJwt',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    publicPath: '/wp-content/themes/bsc-glauchau-theme/_nuxt/',
    extractCSS: true,
  },

  generate: {
    dir: '.dist',
  },
}
