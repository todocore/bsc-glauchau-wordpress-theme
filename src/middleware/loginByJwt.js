export default async function (ctx) {
  await ctx.store.dispatch('auth/loginByJwt', {
    api: ctx.$wp
  })
}
