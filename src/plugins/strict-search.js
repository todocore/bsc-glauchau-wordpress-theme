import Vue from 'vue'

const strictSearch = (needle, haystack) => {
  const hlen = haystack.length
  const nlen = needle.length
  if (nlen > hlen) {
    return false
  }

  needle = needle.toLowerCase()
  haystack = haystack.toLowerCase()

  if (nlen === hlen) {
    return needle === haystack
  }

  return haystack.includes(needle)
}

Vue.prototype.$strictSearch = strictSearch

export default ({ app, }) => {
  app.strictSearch = strictSearch
}
