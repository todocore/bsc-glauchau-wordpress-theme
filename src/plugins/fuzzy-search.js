/* eslint no-labels: ["error", { "allowLoop": true, "allowSwitch": true  }] */
import Vue from 'vue'

const fuzzySearch = (needle, haystack) => {
  /* eslint no-restricted-syntax: ["error", "ForInStatement", "WithStatement"] */
  const hlen = haystack.length
  const nlen = needle.length
  if (nlen > hlen) {
    return false
  }

  needle = needle.toLowerCase()
  haystack = haystack.toLowerCase()

  if (nlen === hlen) {
    return needle === haystack
  }

  outer: for (let i = 0, j = 0; i < nlen; i++) {
    const nch = needle.codePointAt(i)
    while (j < hlen) {
      if (haystack.codePointAt(j++) === nch) {
        continue outer
      }
    }
    return false
  }
  return true
}

Vue.prototype.$fuzzySearch = fuzzySearch

export default ({ app, }) => {
  app.fuzzySearch = fuzzySearch
}
