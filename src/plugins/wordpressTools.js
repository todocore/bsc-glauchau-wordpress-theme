const wordpressToolThumbnail = (ctx, obj = {}, imgSize = 'medium') => {
  const embedded = obj._embedded || {}
  const featuredmedia = embedded['wp:featuredmedia'] || []
  const media = featuredmedia[0] || {}
  const details = media.media_details || {}
  const sizes = details.sizes || {}
  const size = sizes[imgSize] || {}
  return size.source_url || ''
}

const wordpressToolRendered = (ctx, obj = {}) => {
  return obj.rendered || ''
}

const wordpressToolDTF = {
  DD_MM_YYYY: 'dd.MM.yyyy',
  ISO_8601: "yyyy-MM-dd'T'HH:mm:ss",
  ISO_8601_REDUCED: 'yyyy-MM-dd HH:mm:ss',
  YYYY: 'yyyy',
  YYYY_MM_DD: 'yyyy-MM-dd',
  HH_MM: 'HH:mm',
  DAY: 'dd',
  WEEK: 'ww',
}

const wordpressToolDate = (ctx, obj = '', src, dst) => {
  try {
    const srcDate = ctx.$dateFns.parse(obj, src, new Date())
    const dstDate = ctx.$dateFns.format(srcDate, dst)
    return dstDate
  } catch (e) {
    return dst
  }
}

const wordpressToolDateRange = (ctx, fd = '', ld = '', src, dst) => {
  try {
    const fdDate = ctx.$dateFns.parse(fd, src, new Date())
    const ldDate = ctx.$dateFns.parse(ld, src, new Date())
    const range = ctx.$dateFns.eachDayOfInterval({
      start: fdDate,
      end: ldDate,
    })
    return range.map(entry => ctx.$dateFns.format(entry, dst))
  } catch (e) {
    return []
  }
}

export default (ctx, inject) => {
  inject('wpThumbnail', (obj, size) => wordpressToolThumbnail(ctx, obj, size))
  inject('wpRendered', obj => wordpressToolRendered(ctx, obj))
  inject('DTF', wordpressToolDTF)
  inject('wpDate', (obj, src, dst) => wordpressToolDate(ctx, obj, src, dst))
  inject('wpDateRange', (fd, ld, src, dst) => wordpressToolDateRange(ctx, fd, ld, src, dst))

  ctx.$wpThumbnail = wordpressToolThumbnail
  ctx.$wpRendered = wordpressToolRendered
  ctx.$DTF = wordpressToolDTF
  ctx.$wpDate = wordpressToolDate
  ctx.$wpDateRange = wordpressToolDateRange
}
