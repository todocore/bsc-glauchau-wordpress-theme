import { isArray } from '@vue/shared'
import axios from 'axios'

const wordpressApi = async (ctx, endpoint, { method = 'get', route = 'wp/v2', params = [], body = undefined, includePrivateIfLoggedIn = false, }) => {
  const jwt = ctx.store.state.auth.jwt

  if (jwt && includePrivateIfLoggedIn) {
    params.push('status[]=private')
    params.push('status[]=publish')
  }

  const _params = params.join('&')
  const query = _params ? `?${_params}` : ''

  const url = ctx.env.URL_WP_API || ctx.store.state.runtime.URL_WP_API

  const request = {
    method,
    url: `${url}/wp-json/${route}/${endpoint}${query}`,
    data: body,
  }

  if (jwt) {
    request.headers = {
      'X-BSC-Authorization': `Bearer ${jwt}`,
    }
  }

  let response
  try {
    response = await axios(request)
    if (response.data && isArray(response.data)) {
      response.data = response.data.filter((entry) => {
        const protectedByExcerpt = entry.excerpt?.protected === true
        const protectedByContent = entry.content?.protected === true
        return !protectedByExcerpt && !protectedByContent
      })
    }
  } catch (error) {
    if (error.response.status === 403) {
      throw error
    }
    response = error.response
  }

  return response
}

export default (ctx, inject) => {
  inject('wp', (endpoint, params = {}) => wordpressApi(ctx, endpoint, params))
}
