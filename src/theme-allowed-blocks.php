<?php
defined(ALLOW_BSC_THEME) or die();

function bsc_allowed_block_types($allowed_block_types, $editor_context) {
  $user = wp_get_current_user();
  $isAdmin = in_array('administrator', (array)$user->roles);
  if ( !$isAdmin && !empty( $editor_context->post ) ) {
    return array(
      'core/columns',
      'core/file',
      'core/group',
      'core/heading',
      'core/image',
      'core/list',
      'core/paragraph',
      'core/preformatted',
      'core/rows',
      'core/separator',
      'core/spacer',
      'core/table'
    );
  }
  return $allowed_block_types;
}
add_filter('allowed_block_types_all', 'bsc_allowed_block_types', 10, 2);
