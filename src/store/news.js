import { set } from 'vue'

export const state = () => {
  return {
    loading: false,
    entity: undefined,
    entities: [],
    byCategory: [],
    page: 1,
    pages: 1,
  }
}

export const mutations = {
  setValue (state, { key, value, }) {
    set(state, key, value)
  },
  addEntities (state, { values = [], }) {
    set(state, 'entities', state.entities.concat(values))
  },
}

export const getters = {
  hasNextPage (state) {
    return state.page < state.pages
  },
}

export const actions = {
  reset ({ commit, }) {
    commit('setValue', { key: 'loading', value: false, })
    commit('setValue', { key: 'entity', value: undefined, })
    commit('setValue', { key: 'entities', value: [], })
    commit('setValue', { key: 'page', value: 1, })
    commit('setValue', { key: 'pages', value: 1, })
  },

  async load ({ state, commit, }, { api, slug, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    let response

    if (state.entities.length === 0) {
      let entries = []

      // load maximally 3 sticky entities
      response = await api('posts', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          '_fields[]=excerpt',
          '_fields[]=date',
          'sticky=true',
          'order=desc',
          'orderby=date',
          'per_page=3',
        ],
      })
      if (Array.isArray(response.data)) {
        entries = entries.concat(response.data)
      }

      // load first page of latest entities
      response = await api('posts', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          '_fields[]=excerpt',
          '_fields[]=date',
          'sticky=false',
          'order=desc',
          'orderby=date',
          'per_page=6',
          `page=${state.page}`,
        ],
      })
      if (Array.isArray(response.data)) {
        entries = entries.concat(response.data)
        commit('setValue', {
          key: 'pages',
          value: response.headers['x-wp-totalpages'],
        })
      }

      // sanitize all received entities
      commit('setValue', {
        key: 'entities',
        value: entries.map((entry) => {
          return {
            date: entry.date,
            excerpt: this.$wpRendered(entry.excerpt),
            id: entry.id,
            thumbnail: this.$wpThumbnail(entry, 'medium'),
            slug: entry.slug,
            title: this.$wpRendered(entry.title),
            uiDate: this.$wpDate(entry.date, this.$DTF.ISO_8601, this.$DTF.DD_MM_YYYY),
          }
        }),
      })
    }

    if (slug && (!state.entity || state.entity.slug !== slug)) {
      response = await api('posts', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          '_fields[]=excerpt',
          '_fields[]=content',
          '_fields[]=date',
          `slug=${slug}`,
        ],
      })

      const entries = response.data || []
      const entry = entries[0] || {}

      commit('setValue', {
        key: 'entity',
        value: {
          content: this.$wpRendered(entry.content),
          date: entry.date,
          excerpt: this.$wpRendered(entry.excerpt),
          id: entry.id,
          thumbnail: this.$wpThumbnail(entry, 'medium'),
          slug: entry.slug,
          title: this.$wpRendered(entry.title),
          uiDate: this.$wpDate(entry.date, this.$DTF.ISO_8601, this.$DTF.DD_MM_YYYY),
        },
      })
    }

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },

  async loadNextPage ({ state, commit, }, { api, }) {
    if (state.page < state.pages) {
      commit('setValue', {
        key: 'loading',
        value: true,
      })

      const nextPage = state.page + 1

      // load next page of latest entities
      const response = await api('posts', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          '_fields[]=excerpt',
          '_fields[]=date',
          'sticky=false',
          'order=desc',
          'orderby=date',
          'per_page=6',
          `page=${nextPage}`,
        ],
      })

      commit('addEntities', {
        values: (response.data || []).map((entry) => {
          return {
            content: this.$wpRendered(entry.content),
            date: entry.date,
            excerpt: this.$wpRendered(entry.excerpt),
            id: entry.id,
            thumbnail: this.$wpThumbnail(entry, 'medium'),
            slug: entry.slug,
            title: this.$wpRendered(entry.title),
            uiDate: this.$wpDate(entry.date, this.$DTF.ISO_8601, this.$DTF.DD_MM_YYYY),
          }
        }),
      })

      commit('setValue', {
        key: 'page',
        value: nextPage,
      })

      commit('setValue', {
        key: 'loading',
        value: false,
      })
    }
  },

  async loadByCategory ({ state, commit, }, { api, category, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    const response = await api('posts', {
      includePrivateIfLoggedIn: true,
      params: [
        '_embed=wp:featuredmedia',
        '_fields[]=_embedded.wp:featuredmedia',
        '_fields[]=_links.wp:featuredmedia',
        '_fields[]=id',
        '_fields[]=slug',
        '_fields[]=title',
        '_fields[]=excerpt',
        '_fields[]=date',
        `categories=${category}`,
        'order=desc',
        'orderby=date',
        'per_page=100',
      ],
    })

    // sanitize all received entities
    commit('setValue', {
      key: 'byCategory',
      value: (response.data || []).map((entry) => {
        return {
          date: entry.date,
          excerpt: this.$wpRendered(entry.excerpt),
          id: entry.id,
          thumbnail: this.$wpThumbnail(entry, 'medium'),
          slug: entry.slug,
          title: this.$wpRendered(entry.title),
          uiDate: this.$wpDate(entry.date, this.$DTF.ISO_8601, this.$DTF.DD_MM_YYYY),
        }
      }),
    })

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },
}
