const BSC_JWT_KEY = 'BSC_JWT'

export const state = () => {
  return {
    jwt: undefined,
    registrationState: 'INPUT',
    me: undefined,
  }
}

export const mutations = {
  setJwt (state, value) {
    state.jwt = value
  },
  setRegistrationState (state, value) {
    state.registrationState = value
  },
  setMe (state, value) {
    state.me = value
  },
}

export const getters = {
  isAuthenticated (state) {
    const me = state.me || {}
    return !!state.jwt && !!me.id
  },
  uiName (state) {
    const me = state.me || {}
    let name = ''

    name = (name || me.firstName) || ''
    name = (name || me.name) || ''
    name = (name || me.email) || 'Gast'

    return name
  },
  fullName (state) {
    const me = state.me || {}
    const fullName = `${me.firstName || ''} ${me.lastName || ''}`
    return fullName.trim() || (me.name || '') || (me.email || '')
  },
}

export const actions = {
  async registerByCredentials ({ state, commit, }, { api, email, password, authcode, }) {
    commit('setRegistrationState', 'PENDING')
    const response = await api('users', {
      method: 'post',
      route: 'simple-jwt-login/v1',
      body: {
        email,
        password,
        AUTH_KEY: authcode,
      },
    })
    if (response.data?.success === true) {
      commit('setRegistrationState', 'SUCCESS')
    } else {
      commit('setRegistrationState', 'ERROR')
    }
  },

  async loginByCredentials ({ state, commit, dispatch, }, { api, email, password, }) {
    const response = await api('auth', {
      method: 'post',
      route: 'simple-jwt-login/v1',
      body: {
        email,
        password,
      },
    })
    if (response.data.success === true && response.data.data?.jwt) {
      commit('setJwt', response.data.data.jwt)
      localStorage.setItem(BSC_JWT_KEY, state.jwt)
      dispatch('me', { api, })
    } else {
      commit('setMe', undefined)
    }
  },

  async loginByJwt ({ state, commit, dispatch, }, { api, }) {
    const jwt = localStorage.getItem(BSC_JWT_KEY)
    if (jwt && !state.jwt) {
      commit('setJwt', jwt)
      const response = await api('autologin', {
        route: 'simple-jwt-login/v1',
      })
      if (response.data.success === true) {
        dispatch('me', { api, })
      } else {
        commit('setJwt', undefined)
        localStorage.removeItem(BSC_JWT_KEY)
        commit('setMe', undefined)
      }
    }
  },

  async refresh ({ state, commit, dispatch, }, { api, }) {
    const response = await api('auth/refresh', {
      method: 'post',
      route: 'simple-jwt-login/v1',
    })
    if (response.data.success === true && response.data.data?.jwt) {
      commit('setJwt', response.data.data.jwt)
      localStorage.setItem(BSC_JWT_KEY, state.jwt)
      dispatch('me', { api, })
    } else {
      commit('setJwt', undefined)
      localStorage.removeItem(BSC_JWT_KEY)
      commit('setMe', undefined)
    }
  },

  async logout ({ commit, }, { api, }) {
    await api('auth/revoke', {
      method: 'post',
      route: 'simple-jwt-login/v1',
    })
    commit('setJwt', undefined)
    localStorage.removeItem(BSC_JWT_KEY)
    commit('setMe', undefined)
  },

  async me ({ commit, }, { api, }) {
    const response = await api('users/me', {
      method: 'get',
      params: [
        '_fields[]=id',
        '_fields[]=name',
        '_fields[]=first_name',
        '_fields[]=last_name',
        '_fields[]=email',
        'context=edit',
      ],
    })
    if (response.data?.id) {
      commit('setMe', {
        email: response.data.email,
        firstName: response.data.first_name,
        id: response.data.id,
        lastName: response.data.last_name,
        name: response.data.name,
      })
    }
  },
}
