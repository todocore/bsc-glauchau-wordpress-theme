import { set } from 'vue'

export const state = () => {
  return {
    loading: false,
    entity: undefined,
    entities: [],
    byCategory: [],
    page: 1,
    pages: 1,
  }
}

export const mutations = {
  setValue (state, { key, value, }) {
    set(state, key, value)
  },
  addEntities (state, { values = [], }) {
    set(state, 'entities', state.entities.concat(values).sort((a, b) => b.datum.localeCompare(a.datum)))
  },
}

export const getters = {
  hasNextPage (state) {
    return state.page < state.pages
  },
}

export const actions = {
  reset ({ commit, }) {
    commit('setValue', { key: 'loading', value: false, })
    commit('setValue', { key: 'entity', value: undefined, })
    commit('setValue', { key: 'entities', value: [], })
    commit('setValue', { key: 'page', value: 1, })
    commit('setValue', { key: 'pages', value: 1, })
  },

  async load ({ state, commit, }, { api, slug, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    let response

    if (state.entities.length === 0) {
      // load first page of latest entities
      response = await api('galerie', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=bilder',
          '_fields[]=content.protected',
          '_fields[]=datum',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          'per_page=8',
          `page=${state.page}`,
        ],
      })
      if (Array.isArray(response.data)) {
        commit('setValue', {
          key: 'pages',
          value: response.headers['x-wp-totalpages'],
        })
      }

      // sanitize all received entities
      commit('setValue', {
        key: 'entities',
        value: (response.data || [])
          .sort((a, b) => b.datum.localeCompare(a.datum))
          .map((entry) => {
            const bilderList = entry.bilder || []
            const bilder = bilderList.length

            return {
              bilder,
              datum: entry.datum,
              id: entry.id,
              thumbnail: this.$wpThumbnail(entry, 'medium'),
              slug: entry.slug,
              title: this.$wpRendered(entry.title),
              uiDate: this.$wpDate(entry.datum, this.$DTF.YYYY_MM_DD, this.$DTF.DD_MM_YYYY),
            }
          }),
      })
    }

    if (slug && (!state.entity || state.entity.slug !== slug)) {
      response = await api('galerie', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=bilder',
          '_fields[]=content',
          '_fields[]=datum',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          `slug=${slug}`,
        ],
      })

      const entries = response.data || []
      const entry = entries[0] || {}

      const bilder = entry.bilder || []

      const transitionWrapper = entry.transition || []
      const transition = transitionWrapper[0] || 'slide'

      commit('setValue', {
        key: 'entity',
        value: {
          bilder,
          content: this.$wpRendered(entry.content),
          datum: entry.datum,
          id: entry.id,
          thumbnail: this.$wpThumbnail(entry, 'medium'),
          title: this.$wpRendered(entry.title),
          transition,
          uiDate: this.$wpDate(entry.datum, this.$DTF.YYYY_MM_DD, this.$DTF.DD_MM_YYYY),
        },
      })
    }

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },

  async loadNextPage ({ state, commit, }, { api, }) {
    if (state.page < state.pages) {
      commit('setValue', {
        key: 'loading',
        value: true,
      })

      const nextPage = state.page + 1

      // load next page of latest entities
      const response = await api('galerie', {
        includePrivateIfLoggedIn: true,
        params: [
          '_embed=wp:featuredmedia',
          '_fields[]=_embedded.wp:featuredmedia',
          '_fields[]=_links.wp:featuredmedia',
          '_fields[]=content.protected',
          '_fields[]=bilder',
          '_fields[]=datum',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          'per_page=8',
          `page=${nextPage}`,
        ],
      })

      commit('addEntities', {
        values: (response.data || [])
          .map((entry) => {
            const bilderList = entry.bilder || []
            const bilder = bilderList.length

            return {
              bilder,
              datum: entry.datum,
              id: entry.id,
              thumbnail: this.$wpThumbnail(entry, 'medium'),
              slug: entry.slug,
              title: this.$wpRendered(entry.title),
              uiDate: this.$wpDate(entry.datum, this.$DTF.YYYY_MM_DD, this.$DTF.DD_MM_YYYY),
            }
          }),
      })

      commit('setValue', {
        key: 'page',
        value: nextPage,
      })

      commit('setValue', {
        key: 'loading',
        value: false,
      })
    }
  },

  async loadByCategory ({ state, commit, }, { api, category, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    const response = await api('galerie', {
      includePrivateIfLoggedIn: true,
      params: [
        '_embed=wp:featuredmedia',
        '_fields[]=_embedded.wp:featuredmedia',
        '_fields[]=_links.wp:featuredmedia',
        '_fields[]=bilder',
        '_fields[]=content.protected',
        '_fields[]=datum',
        '_fields[]=id',
        '_fields[]=slug',
        '_fields[]=title',
        `categories=${category}`,
        'per_page=100',
      ],
    })

    // sanitize all received entities
    commit('setValue', {
      key: 'byCategory',
      value: (response.data || [])
        .sort((a, b) => b.datum.localeCompare(a.datum))
        .map((entry) => {
          const bilderList = entry.bilder || []
          const bilder = bilderList.length

          return {
            bilder,
            datum: entry.datum,
            id: entry.id,
            thumbnail: this.$wpThumbnail(entry, 'medium'),
            slug: entry.slug,
            title: this.$wpRendered(entry.title),
            uiDate: this.$wpDate(entry.datum, this.$DTF.YYYY_MM_DD, this.$DTF.DD_MM_YYYY),
          }
        }),
    })

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },
}
