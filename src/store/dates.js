import { set } from 'vue'

const MANDATORY = 'MANDATORY'
const DISABLED = 'NONE'

export const state = () => {
  return {
    deadline: '',
    loading: false,
    saving: false,
    entity: undefined,
    entities: [],
    byCategory: [],
    page: 1,
    pages: 1,
    calendarYear: undefined,
    calendarMonth: undefined,
  }
}

export const mutations = {
  setValue (state, { key, value, }) {
    set(state, key, value)
  },
}

export const getters = {
  hasNextPage (state) {
    return state.page < state.pages
  },
  byDate (state) {
    return ({ date, }) => {
      return state.entities
        .filter(entry => (entry.range.includes(date)))
        .sort((a, b) => a.beginn.localeCompare(b.beginn))
    }
  },
  datesToCome (state) {
    return state.entities
      .filter(entry => entry.beginn.localeCompare(state.deadline) >= 0)
      .sort((a, b) => a.beginn.localeCompare(b.beginn))
  },
  datesPassed (state) {
    return state.entities
      .filter(entry => entry.beginn.localeCompare(state.deadline) < 0)
      .sort((a, b) => b.beginn.localeCompare(a.beginn))
  },
  index (state) {
    let result = []
    const iMax = state.entities.length
    for (let i = 0; i < iMax; i++) {
      result = result.concat(state.entities[i].range)
    }
    return result
  },
  uiDate (state) {
    const item = state.entity || {}
    const fd = item.uiFirstDay || ''
    const ld = item.uiLastDay || ''
    const st = item.uiStartTime || ''
    const et = item.uiEndTime || ''

    if (fd.toLowerCase() === ld.toLowerCase()) {
      return `${fd} - ${st} bis ${et} Uhr`
    } else {
      return `${fd} bis ${ld}`
    }
  },
}

export const actions = {
  reset ({ commit, }) {
    commit('setValue', { key: 'deadline', value: '', })
    commit('setValue', { key: 'loading', value: false, })
    commit('setValue', { key: 'entity', value: undefined, })
    commit('setValue', { key: 'entities', value: [], })
    commit('setValue', { key: 'page', value: 1, })
    commit('setValue', { key: 'pages', value: 1, })
    commit('setValue', { key: 'calendarYear', value: undefined, })
    commit('setValue', { key: 'calendarMonth', value: undefined, })
  },

  async load ({ state, commit, }, { api, slug, forceSlug = false, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    const now = this.$dateFns.endOfDay(new Date())
    const deadline = this.$dateFns.format(now, this.$DTF.YYYY_MM_DD) + ' 00:00:00'
    commit('setValue', {
      key: 'deadline',
      value: deadline,
    })

    let response

    if (state.entities.length === 0) {
      let page = state.page
      let pages = state.pages
      let entries = []

      // load first page of latest entities
      response = await api('termin', {
        includePrivateIfLoggedIn: true,
        params: [
          '_fields[]=beginn',
          '_fields[]=content.protected',
          '_fields[]=enabled',
          '_fields[]=ende',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=title',
          'per_page=100',
          `page=${page}`,
        ],
      })
      if (Array.isArray(response.data)) {
        entries = entries.concat(response.data)
        pages = response.headers['x-wp-totalpages']
        commit('setValue', {
          key: 'pages',
          value: pages,
        })
      }

      while (page < pages) {
        page++
        response = await api('termin', {
          includePrivateIfLoggedIn: true,
          params: [
            '_fields[]=beginn',
            '_fields[]=content.protected',
            '_fields[]=enabled',
            '_fields[]=ende',
            '_fields[]=id',
            '_fields[]=slug',
            '_fields[]=title',
            'per_page=100',
            `page=${page}`,
          ],
        })
        if (Array.isArray(response.data)) {
          entries = entries.concat(response.data)
          commit('setValue', {
            key: 'page',
            value: page,
          })
        }
      }

      // sanitize all received entities
      commit('setValue', {
        key: 'entities',
        value: entries.map((entry) => {
          let fd = entry.beginn
          let ld = entry.ende

          if (fd.localeCompare(ld) > 0) {
            fd = entry.ende
            ld = entry.beginn
          }

          return {
            beginn: entry.beginn,
            range: this.$wpDateRange(fd, ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.YYYY_MM_DD),
            id: entry.id,
            slug: entry.slug,
            title: this.$wpRendered(entry.title),
            uiFirstDay: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
            uiLastDay: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
            uiStartTime: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
            uiEndTime: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
          }
        }),
      })
    }

    if (slug && (!state.entity || state.entity.slug !== slug || forceSlug)) {
      response = await api('termin', {
        includePrivateIfLoggedIn: true,
        params: [
          '_fields[]=beginn',
          '_fields[]=content',
          '_fields[]=enabled',
          '_fields[]=ende',
          '_fields[]=id',
          '_fields[]=slug',
          '_fields[]=teilnehmer',
          '_fields[]=telefonnummer',
          '_fields[]=title',
          `slug=${slug}`,
        ],
      })

      const entries = response.data || []
      const entry = entries[0] || {}

      let fd = entry.beginn
      let ld = entry.ende

      if (fd.localeCompare(ld) > 0) {
        fd = entry.ende
        ld = entry.beginn
      }

      const allowAttendees = (entry.enabled === '1' || false)
      const phoneEnabled = DISABLED.localeCompare(entry.telefonnummer || DISABLED) !== 0
      const phoneMandatory = MANDATORY.localeCompare(entry.telefonnummer) === 0

      commit('setValue', {
        key: 'entity',
        value: {
          allowAttendees,
          ask: allowAttendees
            ? {
                phone: {
                  enabled: phoneEnabled,
                  mandatory: phoneMandatory,
                },
              }
            : undefined,
          content: this.$wpRendered(entry.content),
          id: entry.id,
          slug: entry.slug,
          title: this.$wpRendered(entry.title),
          uiFirstDay: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
          uiLastDay: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
          uiStartTime: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
          uiEndTime: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
          teilnehmer: entry.teilnehmer
            ? entry.teilnehmer.map((tn) => {
              return {
                id: tn.ID,
                userId: tn.user,
                name: tn.post_title,
              }
            })
            : undefined,
        },
      })
    }

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },

  loadNextPage ({ state, }, { api, }) {
    // nothing to do
    // ALL dates should have been loaded on initial load
    // this method is only here for compatibility with SearchableListHorizontal.vue
  },

  async registerAttendee ({ state, commit, }, { api, userId, terminId, name, email, phone, }) {
    commit('setValue', {
      key: 'saving',
      value: true,
    })

    if (!!terminId && !!name && !!email) {
      await api('teilnehmer', {
        method: 'post',
        body: {
          termin: terminId,
          title: name,
          email,
          telefonnummer: phone || '',
          status: 'publish',
          user: userId,
        },
      })
    }

    commit('setValue', {
      key: 'saving',
      value: false,
    })
  },

  async loadByCategory ({ state, commit, }, { api, category, }) {
    commit('setValue', {
      key: 'loading',
      value: true,
    })

    const response = await api('termin', {
      includePrivateIfLoggedIn: true,
      params: [
        '_fields[]=beginn',
        '_fields[]=content.protected',
        '_fields[]=enabled',
        '_fields[]=ende',
        '_fields[]=id',
        '_fields[]=slug',
        '_fields[]=title',
        `categories=${category}`,
        'per_page=100',
      ],
    })

    // sanitize all received entities
    commit('setValue', {
      key: 'byCategory',
      value: (response.data || []).map((entry) => {
        let fd = entry.beginn
        let ld = entry.ende

        if (fd.localeCompare(ld) > 0) {
          fd = entry.ende
          ld = entry.beginn
        }

        return {
          beginn: entry.beginn,
          range: this.$wpDateRange(fd, ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.YYYY_MM_DD),
          id: entry.id,
          slug: entry.slug,
          title: this.$wpRendered(entry.title),
          uiFirstDay: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
          uiLastDay: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.DD_MM_YYYY),
          uiStartTime: this.$wpDate(fd, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
          uiEndTime: this.$wpDate(ld, this.$DTF.ISO_8601_REDUCED, this.$DTF.HH_MM),
        }
      }),
    })

    commit('setValue', {
      key: 'loading',
      value: false,
    })
  },
}
