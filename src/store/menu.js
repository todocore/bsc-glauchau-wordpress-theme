export const state = () => {
  return {
    main: undefined,
    footer: undefined,
  }
}

export const mutations = {
  setValue (state, { key, value, }) {
    state[key] = value
  },
}

export const actions = {
  reset ({ commit, dispatch, }, { api, }) {
    commit('setValue', { key: 'main', value: undefined, })
    commit('setValue', { key: 'footer', value: undefined, })
    dispatch('loadMenu', { api, name: 'main', })
    dispatch('loadMenu', { api, name: 'footer', })
  },

  async loadMenu ({ state, commit, }, { api, name, force = false, }) {
    if (!force && state[name]) {
      return
    }

    const response = await api('menu', {
      method: 'GET',
      route: 'bsc/v1',
      params: [
        'name=' + name,
      ],
    })

    commit('setValue', {
      key: name,
      value: (Object.values(response.data) || []).map((item) => {
        if (!item.useHref) {
          switch (item.type) {
            case 'post':
              item.url = `/neuigkeiten${item.url.endsWith('/') ? item.url.substring(0, item.url.length - 1) : item.url}`
              break
          }
        }
        return item
      }),
    })
  },
}
