export const state = () => {
  return {
    URL_WP_API: window.URL_WP_API,
    WP_API_NONCE: window.WP_API_NONCE,
    SHOW_BREAKPOINTS: 'true'.localeCompare(window.SHOW_BREAKPOINTS) === 0 || window.SHOW_BREAKPOINTS.startsWith('<?php'),
    AUTHCODE: window.HASHBASE,
  }
}
