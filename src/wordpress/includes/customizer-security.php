<?php
defined(ALLOW_BSC_THEME) or die();

add_action('customize_register', function ($wp_customize) {
  $wp_customize->add_section('bsc-security-section', array(
    'title' => __('BSC Sicherheitseinstellungen', BSC_TEXT_DOMAIN),
    'priority' => 999,
    'description' => __('Einstellungen um die Sicherheit beim Registrierung und Login zu erhöhen.', BSC_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('bsc-security-authcode-setting', array('default' => ""));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'bsc-securoty-authcode-control', array(
    'label' => __('Ein Auth Code aus dem Simple JWT Login Plugin.', BSC_TEXT_DOMAIN),
    'section' => 'bsc-security-section',
    'settings' => 'bsc-security-authcode-setting',
    'type' => 'password'
  )));
});
