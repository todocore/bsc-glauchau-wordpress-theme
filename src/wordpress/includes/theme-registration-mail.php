<?php
defined(ALLOW_BSC_THEME) or die();

function bsc_registration_mail_subject($title) {
  return "Setze Dein neues Passwort für die Homepage des BSC Glauchau e.V.";
}
add_filter('retrieve_password_title', 'bsc_registration_mail_subject');

function bsc_registration_mail_message($message, $key, $user_login) {
  $url = network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login');
  $msg = "";
  $msg .= "Hallo,\r\n";
  $msg .= "\r\n";
  $msg .= "jemand (Du?) hat Deine E-Mail-Adresse bei uns registriert oder\r\n";
  $msg .= "ein neues Passwort angefordert (Passwort vergessen?).\r\n";
  $msg .= "\r\n";
  $msg .= "Falls diese Aktion nicht von Dir ausgelöst worden ist,\r\n";
  $msg .= "kannst Du diese Mail einfach ignorieren.\r\n";
  $msg .= "\r\n";
  $msg .= "Ansonsten kannst Du Dein neues Passwort über den folgenden Link festlegen:\r\n";
  $msg .= $url."\r\n";
  $msg .= "\r\n";
  $msg .= "\r\n";
  $msg .= "Viele Grüße\r\n";
  $msg .= "Dein BSC Glauchau\r\n";
  $msg .= "\r\n";
  return $msg;
}
add_filter('retrieve_password_message', 'bsc_registration_mail_message', 10, 3);

function bsc_registration_mail( $user ) {
  retrieve_password($user->user_login);
}
add_filter('simple_jwt_login_register_hook', 'bsc_registration_mail');
