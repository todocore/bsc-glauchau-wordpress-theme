<?php
defined(ALLOW_BSC_THEME) or die();

function bsc_login_message( $message ) {
    if ( empty($message) ){
      $msg = "";
      $msg .= "<p style=\"font-size: 1.5rem; font-weight: bold; text-align: center;\">";
      $msg .= "Du bist dabei, Daten der Webseite zu bearbeiten.";
      $msg .= "</p>";
      $msg .= "<br>";
      $msg .= "<p style=\"font-size: 1.25rem; text-align: center;\">";
      $msg .= "Bitte gib dazu nochmals Deine Zugangsdaten ein!";
      $msg .= "</p>";
      return $msg;
    } else {
        return $message;
    }
}
add_filter('login_message', 'bsc_login_message');
