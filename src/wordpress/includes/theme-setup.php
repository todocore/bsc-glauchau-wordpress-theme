<?php
defined(ALLOW_BSC_THEME) or die();

add_action('after_setup_theme', function () {
  add_theme_support('editor-color-palette');
  add_theme_support('editor-font-sizes');
  add_theme_support('editor-styles');
  add_theme_support('html5');
  add_theme_support('post-thumbnails');
  add_theme_support('title-tag');
  add_theme_support('menus');

  add_editor_style('editor-style.css');
});
