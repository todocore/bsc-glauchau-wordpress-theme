<?php
defined(ALLOW_BSC_THEME) or die();

add_filter('pods_field_pick_object_data_params', function ($params) {
  if ($params['name'] == 'pods_meta_teilnehmer') {
    $id = $params['pod']->id;
    $params['options']['_field_object']->pick_where = "(termin.ID IS NULL OR termin.ID='".$id."')";
  }
  return $params;
}, 1, 1);
