<?php
defined(ALLOW_BSC_THEME) or die();

function api_bsc_menu(WP_REST_Request $request) {
  $paramName = $request['name'];
  if (!isset($paramName)) {
    return new WP_Error('missing_parameter', 'missing parameter: name', array( 'status' => 400 ));
  }

  if (!in_array($paramName, array('main', 'footer'))) {
    return new WP_Error('invalid_parameter', 'invalid value for parameter name: '.$paramName, array( 'status' => 400 ));
  }

  $menuSlug = get_theme_mod('bsc-'.$paramName.'-menu-setting');
  $response = wp_get_nav_menu_items($menuSlug);
  if ($response != false) {
    $response = bsc_build_menu_tree($response);
  }

  $wpResponse = new WP_REST_Response($response);
  return $wpResponse;
}
add_action('rest_api_init', function () {
  register_rest_route( 'bsc/v1', '/menu', array(
    'methods' => 'GET',
    'callback' => 'api_bsc_menu'
  ));
});

function bsc_build_menu_tree(array &$flatNav, $parentId = 0) {
  $branch = [];

  $siteUrl = get_site_url();
  if (substr($siteUrl, -1) === '/') {
    $siteUrl = substr($siteUrl, 0, strlen($siteUrl) - 1);
  }

  foreach ($flatNav as &$navItem) {
    if($navItem->menu_item_parent == $parentId) {
      $children = bsc_build_menu_tree($flatNav, $navItem->ID);
      if($children) {
        $navItem->children = $children;
      }

      $entry = array();
      $entry['ID'] = $navItem->ID;
      $entry['title'] = $navItem->title;
      $entry['data'] = $navItem;
      if ($navItem->object == 'custom') {
        $entry['useHref'] = true;
        $entry['url'] = $navItem->url;
      } else {
        $entry['useHref'] = false;
        $entry['url'] = str_replace($siteUrl, '', $navItem->url);
        $entry['type'] = $navItem->object;
      }
      if (is_array($navItem->children)) {
        $entry['children'] = $navItem->children;
      }
      $branch[$navItem->menu_order] = $entry;

      unset($navItem);
    }
  }

  return $branch;
}
