<?php
defined(ALLOW_BSC_THEME) or die();

add_filter('rest_allowed_cors_headers', function($allowed_headers) {
	$allowed_headers[] = 'X-BSC-Authorization';
	return $allowed_headers;
}, 1, 1);
  