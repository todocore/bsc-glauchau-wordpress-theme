<?php
defined(ALLOW_BSC_THEME) or die();

add_action('customize_register', function ($wp_customize) {
  $wp_customize->add_section('development-section', array(
    'title' => __('BSC Theme Entwicklung', BSC_TEXT_DOMAIN),
    'priority' => 999,
    'description' => __('Einstellungen für den Theme-Entwickler (sollten im produktiven Umfeld nicht ohne Grund verwendet werden)', BSC_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('development-breakpoint-helper-setting', array('default' => false));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'development-breakpoint-helper-control', array(
    'label' => __('Aktuellen Bildschirmgröße anzeigen?', BSC_TEXT_DOMAIN),
    'section' => 'development-section',
    'settings' => 'development-breakpoint-helper-setting',
    'type' => 'checkbox'
  )));
});
