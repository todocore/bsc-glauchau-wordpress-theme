<?php
defined(ALLOW_BSC_THEME) or die();

function bsc_remove_protected_from_title($title) {
  // Return only the title portion as defined by %s, not the additional 
  // 'Protected: ' as added in core
  return "%s";
}
add_filter('protected_title_format', 'bsc_remove_protected_from_title');


function bsc_remove_private_from_title($title) {
  // Return only the title portion as defined by %s, not the additional 
  // 'Private: ' as added in core
  return "%s";
}
add_filter('private_title_format', 'bsc_remove_private_from_title');
