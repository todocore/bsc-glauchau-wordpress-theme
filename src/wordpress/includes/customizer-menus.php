<?php
defined(ALLOW_BSC_THEME) or die();

function bsc_register_customizer_menus_section($wp_customize) {
  $bsc_menus = array();
  $menus = get_terms('nav_menu');
  foreach($menus as $menu){
    $bsc_menus[$menu->slug] = $menu->name;
  }
  
  $wp_customize->add_section('bsc-menus-section', array(
    'title' => __('BSC Fontend Menüs', BSC_TEXT_DOMAIN),
    'priority' => 999,
    'description' => __('Einstellung, welche Wordpress-Menüs im Frontend verwendet werden sollen.', BSC_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('bsc-main-menu-setting', array('default' => false));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'bsc-main-menu-control', array(
    'label' => __('Haupt-Menü', BSC_TEXT_DOMAIN),
    'section' => 'bsc-menus-section',
    'settings' => 'bsc-main-menu-setting',
    'type' => 'select',
    'choices' => $bsc_menus
  )));

  $wp_customize->add_setting('bsc-footer-menu-setting', array('default' => false));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'bsc-footer-menu-control', array(
    'label' => __('Footer-Menü', BSC_TEXT_DOMAIN),
    'section' => 'bsc-menus-section',
    'settings' => 'bsc-footer-menu-setting',
    'type' => 'select',
    'choices' => $bsc_menus
  )));
}
add_action('customize_register', 'bsc_register_customizer_menus_section');
