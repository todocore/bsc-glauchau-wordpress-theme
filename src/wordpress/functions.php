<?php
  defined(ALLOW_BSC_THEME) or die();
  
  define('BSC_TEXT_DOMAIN', 'bsc-glauchau');

  include_once get_template_directory().'/includes/api-bsc-menu.php';
  include_once get_template_directory().'/includes/customizer-development.php';
  include_once get_template_directory().'/includes/customizer-menus.php';
  include_once get_template_directory().'/includes/customizer-security.php';
  include_once get_template_directory().'/includes/filter-cors.php';
  include_once get_template_directory().'/includes/filter-pods-teilnehmer.php';
  include_once get_template_directory().'/includes/theme-allowed-blocks.php';
  include_once get_template_directory().'/includes/theme-login-message.php';
  include_once get_template_directory().'/includes/theme-registration-mail.php';
  include_once get_template_directory().'/includes/theme-remove-title-prefix.php';
  include_once get_template_directory().'/includes/theme-setup.php';
?>