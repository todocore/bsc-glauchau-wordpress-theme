#!/bin/bash

#
# THIS SCRIPT SHOULD NOT BE CALLED DIRECTLY
# Use 'npm run generate'. This will run this script inside the nuxt environment.
#

DIST=".dist"
WP_FILES="wordpress"
WP_THEME="bsc-glauchau-theme.dev"
FILE="./style.css"
CACHE="./style.css.new"
NEEDLE="^Version: .*$"
REPLACEMENT="Version: `date +'%Y%m%d-%H%M%S'`"

SKIP_NUXT=""
if [ "$1" == "--skip-nuxt" ]; then
  SKIP_NUXT="skip"
fi

rm -v -f -r ./$WP_THEME && \
mkdir -v ./$WP_THEME || exit 1

if [ -z "$SKIP_NUXT" -o ! -d "$DIST" ]; then
  rm -v -f -r ./$DIST && \
  nuxt generate || exit 2
fi

cd ./$WP_THEME && \
cp -v -r ../$WP_FILES/* . && \
cp -v -r ../$WP_FILES/.[^.]* . && \
cp -v -r ../$DIST/* . && \
cp -v -r ../$DIST/.[^.]* . && \
cp -v -r ./wp-content/themes/bsc-glauchau-theme/_nuxt . && \
rm -v -r ./wp-content && \
EDITORSTYLE=`grep -l '/*! tailwindcss' ./_nuxt/css/*.css` && \
cp -v $EDITORSTYLE ./editor-style.css && \
cat $FILE | sed "s/$NEEDLE/$REPLACEMENT/" > $CACHE && \
mv -v -f $CACHE $FILE && \
mv -v ./index.html ./index.php && \
cd ..
