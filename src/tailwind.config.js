module.exports = {
  content: [
    './components/*.vue',
    './components/**/*.vue',
    './layouts/*.vue',
    './layouts/**/*.vue',
    './pages/*.vue',
    './pages/**/*.vue',
    './app.html',
  ],
  theme: {
    extend: {
      width: {
        'screen-25': 'calc(100vW * 0.25)',
        'screen-50': 'calc(100vW * 0.5)',
        'screen-75': 'calc(100vW * 0.75)',
        'screen-80': 'calc(100vW * 0.8)',
        'screen-90': 'calc(100vW * 0.9)',

        '1/8': 'calc(100% / 8)',
      },
      height: {
        'screen-25': 'calc(100vH * 0.25)',
        'screen-50': 'calc(100vH * 0.5)',
        'screen-75': 'calc(100vH * 0.75)',
        'screen-80': 'calc(100vH * 0.8)',
        'screen-90': 'calc(100vH * 0.9)',
      },
      minHeight: {
        24: '6rem',
        28: '7rem',
        32: '8rem',
        36: '9rem',
        40: '10rem',
      },
      padding: {
        '1/1': '100%',
        '3/4': '133.333333%',
        '4/3': '75%',
        '16/9': '56.25%',
      },
      zIndex: {
        'modal-0': '1000',
        'modal-1': '1010',
        'modal-2': '1020',
        'modal-3': '1030',
        'modal-4': '1040',
        'modal-5': '1050',
        'modal-6': '1060',
        'modal-7': '1070',
        'modal-8': '1080',
        'modal-9': '1090',
      },
    },
  },
  plugins: [require('daisyui'),],
  daisyui: {
    styled: true,
    themes: false,
  },
}
